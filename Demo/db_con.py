#!/usr/bin/env python

import psycopg2

db_connection = psycopg2.connect("dbname=parserDB user=postgres password=1234")

cursor = db_connection.cursor()

templist = [20, int(''), 'MyItem', 30]

cursor.execute('INSERT INTO PlannedEquipment ("number", "product_number", "naziv", "kolicina") VALUES (%s, %s, %s, %s)', (templist[0], templist[1], templist[2], templist[3]))

db_connection.commit()
cursor.close()
db_connection.close()