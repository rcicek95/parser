# Parser
Excel to PostgreSQL parser

# Init
Before starting the Python script, libraries have to be imported:
	"pip3 install xlrd"
	"pip install psycopg2-binary"
	
# Database
The Database folder contains an SQL script that creates a table (1st part of the task)

# Demo
The Demo folder contains a few scripts created for testing and learning, they are not a part of the official and final solution

# Documentation
The Documentation folder contains a PDF with the given task

# Files
"parser.py" is a script that parses data from "PythonTask.xlsx" to a PostgrSQL database (2nd part of the task)