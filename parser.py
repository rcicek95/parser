#!/usr/bin/env python

import xlrd
import psycopg2


#worksheet
workbook = xlrd.open_workbook('PythonTask.xlsx')
worksheet = workbook.sheet_by_name('Popis planirane opreme')


#database connection
db_connection = psycopg2.connect("dbname=postgres user=postgres password=1234")
cursor = db_connection.cursor()


#function that returns the number of colls that are not empty
#i.e. the actual coll number
#ncols returns number of colls, but it includes assumed unnecessary empty colls
def colls_counter():
	cnt = 0
	for i in range(worksheet.ncols): #goes through each coll in the first row
		if (worksheet.cell_type(0, i) not in (xlrd.XL_CELL_EMPTY, xlrd.XL_CELL_BLANK)):
			cnt += 1
	return(cnt)


#takes the value of each cell
def cells_parser(numberofactualcolls):
	templist = []
	firstpass = 1
	for x in range(worksheet.nrows): #redci
		if(firstpass == 1): #first row does not contain the actual data
			firstpass = 0
		else:
			for y in range(numberofactualcolls): #stupci
				if (worksheet.cell_type(x, y) in (xlrd.XL_CELL_EMPTY, xlrd.XL_CELL_BLANK)):
					templist.append(None) #NULL value
				else:
					templist.append(worksheet.cell(x, y).value) #(red, stupac), from 0
			cursor.execute('INSERT INTO PlannedEquipment ("no", "product_number", "naziv", "kolicina") VALUES (%s, %s, %s, %s)', (templist[0], templist[1], templist[2], templist[3]))
			#print(templist)
			templist.clear()


#the main function
def main():
	cells_parser(colls_counter())


#invoking the main function		
main()


#terminating database connection
db_connection.commit()
cursor.close()
db_connection.close()