CREATE TABLE PlannedEquipment (
	NO					INT			PRIMARY KEY
	, PRODUCT_NUMBER	INT
	, NAZIV				CHAR(50)
	, KOLICINA			INT
);

-- SELECT * FROM PlannedEquipment
